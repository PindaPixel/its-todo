# Into the Source: to-do list assignment

A simple to-do list web application using angular and express.

## Getting started

### Using Docker

**Prerequisites**

1. Make sure you have Docker installed and working correctly:

`docker -v`

2. Run the following command for a production server:

`docker compose up`

Alternatively, run the following command for a development server:

`docker compose -f docker-compose.dev.yml up --build`

### Development without Docker

**Prerequisites**

1. Make sure you have Node version 19.4.0

`node -v`

2. Start the client:

```
cd client
npm start
```

### Building manually
