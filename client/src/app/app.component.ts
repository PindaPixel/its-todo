import { Component, OnInit,  } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit 
{
  filter = '';
  title = 'Into the Source - to-do assignment';
  items: [string, boolean][] = this.deserializeItemsFromLocalStorage();

  get filteredItems(): [string, boolean][] {
    if (!this.filter) return this.items;
    return this.items.filter(([title, _]) => title.toLowerCase().indexOf(this.filter.toLowerCase()) > -1);
  }

  
  ngOnInit(): void 
  {
    fetch('').then(result => {

    }).catch(() => {
      console.log('falling back to localStorage.');
      // Toast notification: falling back to local storage
      this.items = this.deserializeItemsFromLocalStorage();
    });
  }

  handleItemUpdate({ index, value }: { index: number, value: [string, boolean] })
  {
    this.items[index] = value;
    this.serializeItemsToLocalStorage();
  }

  handleItemDelete(index: number)
  {
    this.items.splice(index, 1);
    this.serializeItemsToLocalStorage();
  }

  handleItemCreate(item: string) {
    this.items.push([item, false]);
    this.serializeItemsToLocalStorage();
  }

  deserializeItemsFromLocalStorage(): [string, boolean][]
  {
    const serialized = localStorage.getItem('todo-list');
    
    if (!serialized)
      return [];

    
    return JSON.parse(serialized);
  }

  serializeItemsToLocalStorage(): void
  {
    localStorage.setItem('todo-list', JSON.stringify(this.items));
  }
}
