import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {
    @Input() items: [string, boolean][] = [];
    creating: boolean = false;

    @Output() created = new EventEmitter<string>;

    /** Emits the index of the item deleted */
    @Output() deleted = new EventEmitter<number>;

    @Output() updated = new EventEmitter<{ index: number, value: [string, boolean] }>

    handleCheckboxClick(itemIndex: number) 
    {
        const [title, completed] = this.items[itemIndex];

        this.updated.emit(
            {
                index: itemIndex,
                value: [title, !completed]
            }
        )
    }

    handleDeleteClick(itemIndex: number) 
    {
        this.deleted.emit(itemIndex);
    }

    handleAdd(e: SubmitEvent) 
    {
        e.preventDefault();

        const data = new FormData(e.target as HTMLFormElement);

        const title = data.get('title');

        if (!title) return;
        this.created.emit(title.toString());

        this.creating = false;

    }
}
