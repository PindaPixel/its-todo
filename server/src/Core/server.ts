import express from 'express';

const server = express();

export function Controller(baseUrl: string)
{
    return (value: { new(): any }, context: ClassDecoratorContext) => {
        if (context.kind !== 'class') throw new Error(`Controller decorator can only be used on classes, used on ${context.name} of type ${context.kind} instead.`);

        server.get(baseUrl, (request, response) => {
            response.send(context);
        });

        console.log(JSON.stringify(context));

        console.info(`Registered ${context.name} at URL ${baseUrl}.`);

        return value;
    }
}

// TODO: extract params to pass to args (e.g., https://github.com/sindresorhus/type-fest/issues/516)
export function Route(url: string)
{
    return (value: unknown, context: ClassMethodDecoratorContext) => {
        if (context.kind !== 'method') throw new Error(`Route decorator can only be used on class methods, used on ${context.name.toString()} of type ${context.kind} instead.`);

        console.log(JSON.stringify(context));

        console.info(`Registered ${context.name.toString()} at URL ${url}.`)
    }
}

export function start() {
    server.listen(8000);
    console.info('Listening on port 8000.');
}

export function registerController(controller: { new(): any }) {
    new controller();
}