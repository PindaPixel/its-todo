import { Controller, Route } from "../Core/server";
import Todo from '../Models/Todo';

@Controller('/todo')
export default class TodoController 
{
    @Route('/')
    public list(): Todo[] 
    {
        return [new Todo('testing', false)];
    }

    @Route('/:index')
    public get(index: number): Todo 
    {
        return new Todo('testing', false);
    }

    public create(title: string)
    {
        new Todo(title, false);
    }

    public delete(index: number)
    {
        throw new Error('Not implemented.');
    }

    public update(index: number, todo: Todo) 
    {
        throw new Error('Not implemented.');
    }
}