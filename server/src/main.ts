import { start, registerController } from './Core/server';

import TodoController from './Controllers/TodoController';

registerController(TodoController);

start();